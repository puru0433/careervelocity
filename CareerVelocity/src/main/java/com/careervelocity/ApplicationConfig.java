package com.careervelocity;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.ErrorPage;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;

@Configuration
public class ApplicationConfig {
	@Value("${port:8086}")
	private String port;

	@Value("${context:/assay/admin}")
	private String context;
	
	private Set<ErrorPage> pageHandlers;
	
	@Value("${bucketName:cvrepo}")
	private String bucketName;
	
	@PostConstruct
	private void init() {
		pageHandlers = new HashSet<ErrorPage>();
		pageHandlers.add(new ErrorPage(HttpStatus.NOT_FOUND, "/notfound.html"));
		pageHandlers.add(new ErrorPage(HttpStatus.FORBIDDEN, "/forbidden.html"));
	}

	@Bean
	public EmbeddedServletContainerFactory servletContainer() {
		System.out.println("Context loaded");
		TomcatEmbeddedServletContainerFactory factory = new TomcatEmbeddedServletContainerFactory();
		factory.setPort(Integer.valueOf(port));
		factory.setContextPath(context);
		factory.setErrorPages(pageHandlers);
		return factory;
	}


	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public Set<ErrorPage> getPageHandlers() {
		return pageHandlers;
	}

	public void setPageHandlers(Set<ErrorPage> pageHandlers) {
		this.pageHandlers = pageHandlers;
	}

	public String getBucketName() {
		return bucketName;
	}

	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}
	
	
}
