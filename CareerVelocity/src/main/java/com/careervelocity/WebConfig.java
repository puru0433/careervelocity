package com.careervelocity;

import java.io.File;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.thymeleaf.spring4.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.templateresolver.TemplateResolver;

import com.careervelocity.formatter.GraduationFormatter;
import com.careervelocity.formatter.JobCategoryFormatter;
import com.careervelocity.formatter.SkillFormatter;


@Configuration
@ComponentScan("com.assay.institute")
public class WebConfig extends WebMvcConfigurerAdapter {

	
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/login").setViewName("security/login");
	}
	
	@Bean
	public TemplateResolver templateResolver() {
		SpringResourceTemplateResolver resolver = new SpringResourceTemplateResolver();
		resolver.setPrefix("templates/");
		// resolver.setSuffix(".html");
		return resolver;
	}

	@Override
	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
		PageableHandlerMethodArgumentResolver resolver = new PageableHandlerMethodArgumentResolver();
		resolver.setFallbackPageable(new PageRequest(0, 1));
		resolver.setPageParameterName("page.page");
		resolver.setSizeParameterName("page.size");
		resolver.setOneIndexedParameters(true);
		argumentResolvers.add(resolver);
	}
	
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		String rootPath = System.getProperty("user.home"); 
		if (rootPath != null) {
			String imagePath = "file:" + rootPath + File.separator + "cvrepo/";
			System.out.println(imagePath);
			File dir = new File(rootPath);
			if (!dir.exists()) {
				dir.mkdirs();
			}
			registry.addResourceHandler("/cvrepo/**").addResourceLocations(imagePath);
		}
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	}

	@Override
	public void addFormatters(FormatterRegistry formatterRegistry) {
		formatterRegistry.addFormatter(new JobCategoryFormatter());
		formatterRegistry.addFormatter(new SkillFormatter());
		formatterRegistry.addFormatter(new GraduationFormatter());
	}
}