package com.careervelocity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.careervelocity"})
@EntityScan("com.careervelocity")
public class CareerVelocityApplication {

	public static void main(String[] args) {
		SpringApplication.run(CareerVelocityApplication.class, args);
	}
}
