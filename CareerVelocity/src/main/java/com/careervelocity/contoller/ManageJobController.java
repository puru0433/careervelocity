package com.careervelocity.contoller;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import com.careervelocity.ApplicationConfig;
import com.careervelocity.common.PageWrapper;
import com.careervelocity.models.ErrorMessage;
import com.careervelocity.models.JobSets;
import com.careervelocity.models.TestiMonial;
import com.careervelocity.models.UserProfile;
import com.careervelocity.repository.JobsRepository;
import com.careervelocity.repository.RegistrationRepository;
import com.careervelocity.repository.TestiMonialRepository;

@Controller
public class ManageJobController {
	
	@Autowired
	JobsRepository jobsRepository;
	
	@Autowired
	RegistrationRepository registrationRepository;
	
	@Autowired
	ApplicationConfig applicationConfig;
	
	@Autowired
	TestiMonialRepository testiMonialRepository;
	
	
	@ModelAttribute("testimonials")
	public List<TestiMonial> getAllTestiMonials(){
		return testiMonialRepository.findAll();
	}
	
	@ModelAttribute("user_profile")
	public UserProfile getUserProfile(){
		return new UserProfile();
	}
	
	
	@ModelAttribute("page")
	public PageWrapper<JobSets> getAllJobs(HttpServletRequest request, Pageable pageable){
		PageWrapper<JobSets> page = new PageWrapper<JobSets>(getPageableJobs(request,pageable),"/index");
		return page;
	}
	
	private Page<JobSets> getPageableJobs(HttpServletRequest request,Pageable pageable){
		Page<JobSets> findAll = jobsRepository.findAll(pageable);
		HttpSession session = request.getSession();
		
		String emailId = (String)session.getAttribute("userEmail");
		 Object user = session.getAttribute("user");
		if(emailId != null && user!= null && (boolean)user){
			UserProfile findByUserEmail = registrationRepository.findByUserEmail(emailId);
			if(findByUserEmail != null){
			List<JobSets> appliedJobs = findByUserEmail.getAppliedJobs();
			findAll.forEach(item->{
				if(appliedJobs.contains(item))
				item.setApplied(true);
				else
				item.setApplied(false);
				});
			}
		}
		return findAll;
	}
	
	
	@ModelAttribute("all_jobs")
	public List<JobSets> getAllJobs(HttpServletRequest request){
		List<JobSets> findAll = jobsRepository.findAll();
		HttpSession session = request.getSession();
		
		String emailId = (String)session.getAttribute("userEmail");
		 Object user = session.getAttribute("user");
		if(emailId != null && user!= null && (boolean)user){
			UserProfile findByUserEmail = registrationRepository.findByUserEmail(emailId);
			if(findByUserEmail != null){
			List<JobSets> appliedJobs = findByUserEmail.getAppliedJobs();
			findAll.forEach(item->{
				if(appliedJobs.contains(item))
				item.setApplied(true);
				else
				item.setApplied(false);
				});
			}
		}
		return findAll;
	}
	
	@RequestMapping(value="/manage_job",method=RequestMethod.GET)
	public String getManageJob(){
		return "manage_job";
	}
	
	@RequestMapping(value="/delete_job",method=RequestMethod.POST)
	public String deleteJob(@ModelAttribute JobSets jobSet){
		jobsRepository.deleteByJobSetId(jobSet.getJobSetId());
		return "manage_job";
	}
	
	@RequestMapping(value="/job_listing",method=RequestMethod.GET)
	public String getJobListing(){
		return "job_listing";
	}
	
	@RequestMapping(value="view_job",method=RequestMethod.POST)
	public String getViewJobPage(@ModelAttribute JobSets allJobs,Model model){
		JobSets findById = jobsRepository.findByJobSetId(allJobs.getJobSetId());
		if(findById != null){
			model.addAttribute("job", findById);
			return "job";
		}else
		return "job_listing";
	}
	
	@RequestMapping(value="apply_job",method=RequestMethod.POST)
	public String applyJob(@ModelAttribute JobSets job,HttpServletRequest request,Model model){
		HttpSession session = request.getSession();
		Object object = (Object)session.getAttribute("user");
		if(object !=null && (boolean)object){
			String emailId = (String)session.getAttribute("userEmail");
			UserProfile findByUserEmail = registrationRepository.findByUserEmail(emailId);
			findByUserEmail.getAppliedJobs().add(job);
			registrationRepository.save(findByUserEmail);
			model.addAttribute("all_jobs",getAllJobs(request));
			return addErrorMessage(model,"Information","Applied Successfully","job_listing");
		}else{
			session.setAttribute("applying_job", job.getJobSetId());
			return "login";
		}
	}
	
	@RequestMapping(value="view_job_applications",method=RequestMethod.POST)
	public String getJobApplications(@ModelAttribute JobSets job,Model model){
		List<JobSets> jobList =  new ArrayList<JobSets>();
		JobSets findById = jobsRepository.findByJobSetId(job.getJobSetId());
		jobList.add(findById);
		List<UserProfile> findByAppliedJobs = registrationRepository.findByAppliedJobs(jobList);
		model.addAttribute("user_profiles",findByAppliedJobs);
		return "view_job_applications";
	}
	
	 @RequestMapping(value = "download_resume", method = RequestMethod.POST)
	    public StreamingResponseBody getSteamingFile(@ModelAttribute UserProfile userProfile,HttpServletResponse response) throws IOException {
		 UserProfile findById = registrationRepository.findByUserId(userProfile.getUserId());
		 if(findById!=null){
		 String fileName=findById.getUserName();
		 if(findById.getResumePath().endsWith(".pdf")){
	        response.setContentType("application/pdf");
	        fileName+=".pdf";
		 }
		 else if(findById.getResumePath().endsWith(".doc") ){
			 response.setContentType("Application/msword");
			 fileName+=".doc";
		 } else if( findById.getResumePath().endsWith(".docx")){
			 response.setContentType("Application/msword");
			 fileName+=".docx";
		 }
		 else if( findById.getResumePath().endsWith(".rtf")){
			 response.setContentType("Application/msword");
			 fileName+=".rtf";
		 }else if(findById.getResumePath().endsWith(".txt")){
			 response.setContentType("text/plain");
			 fileName+=".txt";
		 }
	        response.setHeader("Content-Disposition", "attachment; filename="+fileName);
			String filePath = System.getProperty("user.home");
			if(findById.getResumePath() != null){
	        InputStream inputStream = new FileInputStream(new File(filePath + File.separator +findById.getResumePath()));
	        return outputStream -> {
	            int nRead;
	            byte[] data = new byte[1024];
	            while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
	                System.out.println("Writing some bytes..");
	                outputStream.write(data, 0, nRead);
	            }
	        };
			}
		 }
			return null;
	    }
	
	private String addErrorMessage(Model model,String title,String message,String page) {
		ErrorMessage errorMessage = new ErrorMessage();
		errorMessage.setMessage(message);
		errorMessage.setTitle(title);
		model.addAttribute("errorMessage", errorMessage);
		return page;
	}
}
