package com.careervelocity.contoller;

import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.careervelocity.common.PageWrapper;
import com.careervelocity.models.ErrorMessage;
import com.careervelocity.models.JobSets;
import com.careervelocity.models.Services;
import com.careervelocity.models.TestiMonial;
import com.careervelocity.models.UserProfile;
import com.careervelocity.repository.JobsRepository;
import com.careervelocity.repository.RegistrationRepository;
import com.careervelocity.repository.ServiceRepository;
import com.careervelocity.repository.TestiMonialRepository;

@Controller
public class LoginController {
	
	@Autowired
	RegistrationRepository registrationRepository;
	
	@Autowired
	JobsRepository jobsRepository;
	
	@Autowired
	ServiceRepository serviceRepository;
	
	@Autowired
	TestiMonialRepository testiMonialRepository;
	
	
	@ModelAttribute("testimonials")
	public List<TestiMonial> getAllTestiMonials(){
		return testiMonialRepository.findAll();
	}
	
	@ModelAttribute("service_list")
	public List<Services> getServiceList(){
		return serviceRepository.findAll();
	}
	
	@ModelAttribute("page")
	public PageWrapper<JobSets> getAllJobs(HttpServletRequest request, Pageable pageable){
		PageWrapper<JobSets> page = new PageWrapper<JobSets>(getPageableJobs(request,pageable),"/index");
		return page;
	}
	private List<JobSets> getAllJobs(HttpServletRequest request) {
		List<JobSets> findAll = jobsRepository.findAll();
		HttpSession session = request.getSession();
		
		String emailId = (String)session.getAttribute("userEmail");
		 Object user = session.getAttribute("user");
		if(emailId != null && user!= null && (boolean)user){
			UserProfile findByUserEmail = registrationRepository.findByUserEmail(emailId);
			if(findByUserEmail != null){
			List<JobSets> appliedJobs = findByUserEmail.getAppliedJobs();
			findAll.forEach(item->{
				if(appliedJobs.contains(item))
				item.setApplied(true);
				else
				item.setApplied(false);
				});
			}
		}
		return findAll;
	}
	private Page<JobSets> getPageableJobs(HttpServletRequest request,Pageable pageable){
		Page<JobSets> findAll = jobsRepository.findAll(pageable);
		HttpSession session = request.getSession();
		
		String emailId = (String)session.getAttribute("userEmail");
		 Object user = session.getAttribute("user");
		if(emailId != null && user!= null && (boolean)user){
			UserProfile findByUserEmail = registrationRepository.findByUserEmail(emailId);
			if(findByUserEmail != null){
			List<JobSets> appliedJobs = findByUserEmail.getAppliedJobs();
			findAll.forEach(item->{
				if(appliedJobs.contains(item))
				item.setApplied(true);
				else
				item.setApplied(false);
				});
			}
		}
		return findAll;
	}
	
	@ModelAttribute("user_profile")
	public UserProfile getUserProfile(HttpServletRequest request){
		HttpSession session = request.getSession();
		String emailId = (String)session.getAttribute("userEmail");
		if(emailId != null){
			return registrationRepository.findByUserEmail(emailId);
		}
		return new UserProfile();
	}
	
	@RequestMapping(value="/login",method=RequestMethod.GET)
	public String getLogin(){
			return "login";
	}
	
	@RequestMapping(value="/logout",method=RequestMethod.GET)
	public String getLogin(HttpServletRequest request,Model model,Pageable pageable){
		Enumeration<String> attributeNames = request.getSession().getAttributeNames();
		while(attributeNames.hasMoreElements()){
			request.getSession().removeAttribute(attributeNames.nextElement());
		}
		PageWrapper<JobSets> page = new PageWrapper<JobSets>(getPageableJobs(request,pageable),"/index");
		model.addAttribute("page",page);
		return "index";
	}
	
	@RequestMapping(value="/login_user",method=RequestMethod.POST)
	public String checkLogin(@ModelAttribute UserProfile userProfile,Model model,HttpServletRequest request,Pageable pageable){
		HttpSession session = null;
		 session = request.getSession();
		if(checkAdminUser(userProfile)){
			session.setAttribute("admin", true);
			session.setAttribute("user", false);
			session.setAttribute("active", true);
			session.setAttribute("userEmail",userProfile.getUserEmail());
			session.setAttribute("userName","Admin");
			PageWrapper<JobSets> page = new PageWrapper<JobSets>(getPageableJobs(request,pageable),"/index");
			model.addAttribute("page",page);
			return "index";
		}else{
			
		UserProfile found = registrationRepository.findByUserEmail(userProfile.getUserEmail());
		if(found != null){
			if(found.getPassword().equals(userProfile.getPassword())){
				session.setAttribute("user", true);
				session.setAttribute("admin", false);
				session.setAttribute("active", true);
				session.setAttribute("userEmail",found.getUserEmail());
				session.setAttribute("userName",found.getUserName());
				model.addAttribute("all_jobs",getAllJobs(request));
				Long id = (Long)session.getAttribute("applying_job");
				if(id != null){
					JobSets findById = jobsRepository.findByJobSetId(id);
					if(findById != null){
						model.addAttribute("job", findById);
						return "job";
					}
				}
				Services obj = (Services)session.getAttribute("purchasing services");
				if(obj!=null){
					return "services";
				}
				PageWrapper<JobSets> page = new PageWrapper<JobSets>(getPageableJobs(request,pageable),"/index");
				model.addAttribute("page",page);
				return "index";
			}else{
				return addErrorMessage(model,"Error","Invalid Password","login");
			}
			
		}else{
			return addErrorMessage(model,"Error","Email is Not Registered","login");
		}
		}
		
	}
	@RequestMapping(value="/forgot_password",method=RequestMethod.GET)
	public String getForgotPasswordpage(){
		return "forgot_password";
	}
	
	@RequestMapping(value="/forgot_password",method=RequestMethod.POST)
	public String forgotPassword(@ModelAttribute UserProfile userProfile,Model model){
		UserProfile found = registrationRepository.findByUserEmail(userProfile.getUserEmail());
		if(found != null){
			// send mail
			return addErrorMessage(model,"Information","Sent password to your registered mail id","login");
		}else{
			return addErrorMessage(model,"Error","Email is Not Registered","forgot_password");
		}
	}
	private boolean checkAdminUser(UserProfile userProfile) {
		if(userProfile.getUserEmail().equals("admin@gmail.com") && userProfile.getPassword().equals("admin") )
		return true;
		return false;
	}
	private String addErrorMessage(Model model,String title,String message,String page) {
		ErrorMessage errorMessage = new ErrorMessage();
		errorMessage.setMessage(message);
		errorMessage.setTitle(title);
		model.addAttribute("errorMessage", errorMessage);
		return page;
	}
}
