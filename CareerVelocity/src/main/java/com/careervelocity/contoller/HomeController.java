package com.careervelocity.contoller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.careervelocity.common.LoggerUtility;
import com.careervelocity.common.PageWrapper;
import com.careervelocity.models.Graduation;
import com.careervelocity.models.JobCategory;
import com.careervelocity.models.JobSets;
import com.careervelocity.models.Skills;
import com.careervelocity.models.TestiMonial;
import com.careervelocity.models.University;
import com.careervelocity.models.UserProfile;
import com.careervelocity.repository.GraduationRepository;
import com.careervelocity.repository.JobCategoryRepository;
import com.careervelocity.repository.JobsRepository;
import com.careervelocity.repository.RegistrationRepository;
import com.careervelocity.repository.SkillsRepository;
import com.careervelocity.repository.TestiMonialRepository;
import com.careervelocity.repository.UniversityRepository;

@Controller
public class HomeController {

	@Autowired
	SkillsRepository skillRepository;
	
	@Autowired
	JobCategoryRepository jobCategoryRepository;
	
	@Autowired
	JobsRepository jobsRepository;
	
	@Autowired
	RegistrationRepository registrationRepository;
	
	@Autowired
	GraduationRepository graduationRepository;

	@Autowired
	private UniversityRepository universityRepsoitory;
	
	@Autowired
	TestiMonialRepository testiMonialRepository;
	
	
	@ModelAttribute("testimonials")
	public List<TestiMonial> getAllTestiMonials(){
		LoggerUtility.getInstance().logger.info("finding all testimonials");
		return testiMonialRepository.findAll();
	}
	
	@ModelAttribute("page")
	public PageWrapper<JobSets> getAllJobs(HttpServletRequest request, Pageable pageable){
		PageWrapper<JobSets> page = new PageWrapper<JobSets>(getPageableJobs(request,pageable),"/index");
		return page;
	}

	private List<JobSets> getAllJobs(HttpServletRequest request) {
		LoggerUtility.getInstance().logger.info("getting all jobs");
		List<JobSets> findAll = jobsRepository.findAll();
		HttpSession session = request.getSession();
		
		String emailId = (String)session.getAttribute("userEmail");
		 Object user = session.getAttribute("user");
		if(emailId != null && user!= null && (boolean)user){
			UserProfile findByUserEmail = registrationRepository.findByUserEmail(emailId);
			if(findByUserEmail != null){
			List<JobSets> appliedJobs = findByUserEmail.getAppliedJobs();
			findAll.forEach(item->{
				if(appliedJobs.contains(item))
				item.setApplied(true);
				else
				item.setApplied(false);
				});
			}
		}
		return findAll;
	}
	private Page<JobSets> getPageableJobs(HttpServletRequest request,Pageable pageable){
		LoggerUtility.getInstance().logger.info("getting pageable jobs");
		Page<JobSets> findAll = jobsRepository.findAll(pageable);
		HttpSession session = request.getSession();
		
		String emailId = (String)session.getAttribute("userEmail");
		 Object user = session.getAttribute("user");
		if(emailId != null && user!= null && (boolean)user){
			UserProfile findByUserEmail = registrationRepository.findByUserEmail(emailId);
			if(findByUserEmail != null){
			List<JobSets> appliedJobs = findByUserEmail.getAppliedJobs();
			findAll.forEach(item->{
				if(appliedJobs.contains(item))
				item.setApplied(true);
				else
				item.setApplied(false);
				});
			}
		}
		return findAll;
	}
	@RequestMapping(value="/",method=RequestMethod.GET)
	public String index(Pageable pageable, Model model,HttpServletRequest request){
		LoggerUtility.getInstance().logger.info("index page invoked");
		PageWrapper<JobSets> page = new PageWrapper<JobSets>(getPageableJobs(request,pageable),"/index");
		model.addAttribute("page",page);
		return "index";
	}
	
	@RequestMapping(value="/index",method=RequestMethod.GET)
	public String getIndexPage(Pageable pageable, Model model,HttpServletRequest request){
		LoggerUtility.getInstance().logger.info("index page invoked-->");
		PageWrapper<JobSets> page = new PageWrapper<JobSets>(getPageableJobs(request,pageable),"/index");
		model.addAttribute("page",page);
		return "index";
	}
	
	@RequestMapping(value="/about",method=RequestMethod.GET)
	public String getAboutPage(){
		LoggerUtility.getInstance().logger.info("about page invoked");
		return "about";
	}
	
	
	
	@RequestMapping(value="/job",method=RequestMethod.GET)
	public String getJob(){
		LoggerUtility.getInstance().logger.info("job page invoked");
		return "job";
	}
	
	
	
	@PostConstruct
	public void staticInsert(){
		LoggerUtility.getInstance().logger.info("Inserting static values");
		if(skillRepository.findAll().isEmpty()){
		List<Skills> SkillSet = new ArrayList<Skills>(); 
		Skills skill = new Skills();
		skill.setSkillName("Java");
		Skills skill1 = new Skills();
		skill1.setSkillName("C");
		Skills skill2 = new Skills();
		skill2.setSkillName("C++");
		Skills skill3 = new Skills();
		skill3.setSkillName("C#");
		SkillSet.add(skill);
		SkillSet.add(skill1);
		SkillSet.add(skill2);
		SkillSet.add(skill3);
		
		skillRepository.save(SkillSet);
		}
		
		if(jobCategoryRepository.findAll().isEmpty()){
		
		List<JobCategory> jobCategorySet = new ArrayList<JobCategory>();
		JobCategory jobCategory = new JobCategory();
		jobCategory.setCategory("category1");
		JobCategory jobCategory1 = new JobCategory();
		jobCategory1.setCategory("category2");
		JobCategory jobCategory2 = new JobCategory();
		jobCategory2.setCategory("category3");
		JobCategory jobCategory3 = new JobCategory();
		jobCategory3.setCategory("category4");
		
		jobCategorySet.add(jobCategory);
		jobCategorySet.add(jobCategory1);
		jobCategorySet.add(jobCategory2);
		jobCategorySet.add(jobCategory3);
		jobCategoryRepository.save(jobCategorySet);
		
		}
		if(graduationRepository.findAll().isEmpty()){
		List<Graduation> graduationList = new ArrayList<Graduation>();
		Graduation graduation1 = new Graduation();
		graduation1.setGraduationName("BE");
		
		Graduation graduation2 = new Graduation();
		graduation2.setGraduationName("MTech");
		
		graduationList.add(graduation1);
		graduationList.add(graduation2);
		
		graduationRepository.save(graduationList);
		}
		
		if(universityRepsoitory.findAll().isEmpty()){
			List<University> universityList = new ArrayList<University>();
			University university = new University();
			university.setUniversityName("VTU");
			University university1 = new University();
			university1.setUniversityName("ATRIA");
			universityList.add(university);
			universityList.add(university1);
			universityRepsoitory.save(universityList);
		}
	}
	

}
