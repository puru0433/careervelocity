package com.careervelocity.contoller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.careervelocity.models.ErrorMessage;
import com.careervelocity.models.Services;
import com.careervelocity.models.TestiMonial;
import com.careervelocity.models.UserProfile;
import com.careervelocity.repository.RegistrationRepository;
import com.careervelocity.repository.ServiceRepository;
import com.careervelocity.repository.TestiMonialRepository;

@Controller
public class ServicesController {

	@Autowired
	ServiceRepository serviceRepository;
	
	@Autowired
	TestiMonialRepository testiMonialRepository;
	
	@Autowired
	RegistrationRepository registrationRepository;
	
	 
	@ModelAttribute("testimonials")
	public List<TestiMonial> getAllTestiMonials(){
		return testiMonialRepository.findAll();
	}
	
	@RequestMapping(value="services",method=RequestMethod.GET)
	public String getServicePage(HttpServletRequest request){
		return "services";
	}
	
	@ModelAttribute("subscribed")
	public List<Services> subscibedList(HttpServletRequest request){
		HttpSession session = request.getSession();
		String emailId = (String)session.getAttribute("userEmail");
		 UserProfile findByUserEmail = registrationRepository.findByUserEmail(emailId);
		 if(findByUserEmail==null){
			return new ArrayList<Services>(); 
		 }
		return findByUserEmail.getServices()!=null ?findByUserEmail.getServices():new ArrayList<Services>();
	}
	
	@RequestMapping(value="purchase",method=RequestMethod.POST)
	public String purchaseService(HttpServletRequest request,@ModelAttribute Services service,Model model){
		HttpSession session = request.getSession();
		String emailId = (String)session.getAttribute("userEmail");
		 UserProfile findByUserEmail = registrationRepository.findByUserEmail(emailId);
		 String[] split = service.getServiceName().split("-");
		 List<Long> serviceIds = new ArrayList<Long>();
		 for(String item:split){
			 serviceIds.add(Long.parseLong(item));
		 }
		 List<Services> findByServiceIdIn = serviceRepository.findByServiceIdIn(serviceIds);
		 findByUserEmail.setServices(findByServiceIdIn);
		 registrationRepository.save(findByUserEmail);
		 model.addAttribute("subscribed",subscibedList(request));
		 return addErrorMessage( model, "Information", "Subscribed Successfully","services");
	}
	
	
	@ModelAttribute("service_list")
	public List<Services> getServiceList(){
		return serviceRepository.findAll();
	}
	
	@RequestMapping(value="add_services",method=RequestMethod.POST)
	public String addServices(@ModelAttribute Services services,Model model){
		serviceRepository.save(services);
		model.addAttribute("service_list", getServiceList());
		return "services";
	}
	
	@RequestMapping(value="update_service",method=RequestMethod.POST)
	public String updateServices(@ModelAttribute Services services,Model model){
		if(services.getIsActive() == null){
			services.setActive(false);
		}
		serviceRepository.save(services);
		model.addAttribute("service_list", getServiceList());
		return "services";
	}
	
	@RequestMapping(value="delete_service",method=RequestMethod.POST)
	public String deleteServices(@ModelAttribute Services services,Model model){
		serviceRepository.deleteByServiceId(services.getServiceId());
		model.addAttribute("service_list", getServiceList());
		return "services";
	}
	
	private String addErrorMessage(Model model,String title,String message,String page) {
		ErrorMessage errorMessage = new ErrorMessage();
		errorMessage.setTitle(title);
		errorMessage.setMessage(message);
		model.addAttribute("errorMessage", errorMessage);
		return page;
	}
	
	
}
