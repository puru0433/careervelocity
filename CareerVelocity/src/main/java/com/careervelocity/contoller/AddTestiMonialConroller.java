package com.careervelocity.contoller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.careervelocity.common.PageWrapper;
import com.careervelocity.models.ErrorMessage;
import com.careervelocity.models.JobSets;
import com.careervelocity.models.TestiMonial;
import com.careervelocity.models.UserProfile;
import com.careervelocity.repository.JobsRepository;
import com.careervelocity.repository.RegistrationRepository;
import com.careervelocity.repository.TestiMonialRepository;


@Controller
public class AddTestiMonialConroller {
	
	@Autowired
	JobsRepository jobsRepository;
	
	@Autowired
	TestiMonialRepository testiMonialRepository;
	
	@Autowired
	RegistrationRepository registrationRepository;
	
	@ModelAttribute("testimonials")
	public List<TestiMonial> getAllTestiMonials(){
		return testiMonialRepository.findAll();
	}
	
	@ModelAttribute("page")
	public PageWrapper<JobSets> getAllJobs(HttpServletRequest request, Pageable pageable){
		PageWrapper<JobSets> page = new PageWrapper<JobSets>(getPageableJobs(request,pageable),"/index");
		return page;
	}
	
	private Page<JobSets> getPageableJobs(HttpServletRequest request,Pageable pageable){
		Page<JobSets> findAll = jobsRepository.findAll(pageable);
		HttpSession session = request.getSession();
		
		String emailId = (String)session.getAttribute("userEmail");
		 Object user = session.getAttribute("user");
		if(emailId != null && user!= null && (boolean)user){
			UserProfile findByUserEmail = registrationRepository.findByUserEmail(emailId);
			if(findByUserEmail != null){
			List<JobSets> appliedJobs = findByUserEmail.getAppliedJobs();
			findAll.forEach(item->{
				if(appliedJobs.contains(item))
				item.setApplied(true);
				else
				item.setApplied(false);
				});
			}
		}
		return findAll;
	}
	
	@ModelAttribute("add_testimonial")
	public TestiMonial getTestiMonial(){
		return new TestiMonial();
	}
	
	@RequestMapping(value="/add-testimonials",method=RequestMethod.GET)
	public String getAddTestimonial(){
		System.out.println("Index called");
		return "add-testimonials";
	}
	
	@RequestMapping(value="/insert_testimonial",method=RequestMethod.POST)
	public String insertTestiMonial(@ModelAttribute TestiMonial testiMonial,Model model,HttpServletRequest request){
		
		HttpSession session = request.getSession();
		Object attribute = session.getAttribute("userEmail");
		if(attribute!=null){
			testiMonial.setUserName((String)attribute);
		}
		testiMonial.setShowTestimonial(false);
		testiMonialRepository.save(testiMonial);
		addErrorMessage("Added Succesfully", model);
		return "add-testimonials";
	}
	
	private void addErrorMessage(String message,Model model){
		ErrorMessage errorMessage = new ErrorMessage();
		errorMessage.setMessage(message);
		errorMessage.setTitle("Information");
		model.addAttribute("errorMessage",errorMessage);
	}
}
