package com.careervelocity.contoller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.careervelocity.common.PageWrapper;
import com.careervelocity.models.JobSets;
import com.careervelocity.models.TestiMonial;
import com.careervelocity.models.UserProfile;
import com.careervelocity.models.wrappers.TestiMonailWrapper;
import com.careervelocity.repository.JobsRepository;
import com.careervelocity.repository.RegistrationRepository;
import com.careervelocity.repository.TestiMonialRepository;

@Controller
public class ManageTestimonial {

	@Autowired
	TestiMonialRepository testiMonialRepository;
	
	@Autowired
	JobsRepository jobsRepository;
	
	@Autowired
	RegistrationRepository registrationRepository;
	
	@ModelAttribute("testimonials")
	public List<TestiMonial> getAllTestiMonials(){
		return testiMonialRepository.findAll();
	}
	
	@ModelAttribute("page")
	public PageWrapper<JobSets> getAllJobs(HttpServletRequest request, Pageable pageable){
		PageWrapper<JobSets> page = new PageWrapper<JobSets>(getPageableJobs(request,pageable),"/index");
		return page;
	}
	
	private Page<JobSets> getPageableJobs(HttpServletRequest request,Pageable pageable){
		Page<JobSets> findAll = jobsRepository.findAll(pageable);
		HttpSession session = request.getSession();
		
		String emailId = (String)session.getAttribute("userEmail");
		 Object user = session.getAttribute("user");
		if(emailId != null && user!= null && (boolean)user){
			UserProfile findByUserEmail = registrationRepository.findByUserEmail(emailId);
			if(findByUserEmail != null){
			List<JobSets> appliedJobs = findByUserEmail.getAppliedJobs();
			findAll.forEach(item->{
				if(appliedJobs.contains(item))
				item.setApplied(true);
				else
				item.setApplied(false);
				});
			}
		}
		return findAll;
	}
	
	
	@ModelAttribute("testi_monials")
	public TestiMonailWrapper getTestiMonial(){
		TestiMonailWrapper testiMonialWrapper = new TestiMonailWrapper();
		 testiMonialWrapper.setTestiMonialList(testiMonialRepository.findAll());
		 return testiMonialWrapper;
	}
	@RequestMapping(value="manage_testimonial", method=RequestMethod.GET)
	public String getManageTesimonial(){
		return "manage_testimonial";
	}
	
	
	@RequestMapping(value="updateTestimonals",method=RequestMethod.POST)
	public String updateTestiMonial(@ModelAttribute TestiMonailWrapper testiMonialWrapper,Model model){
		List<TestiMonial> testiMonialList = testiMonialWrapper.getTestiMonialList();
		testiMonialList.forEach(item->{
			if(item.getTestiMonialId() != null){
				TestiMonial testiMonial = testiMonialRepository.findByTestiMonialId(item.getTestiMonialId());
				testiMonial.setShowTestimonial(item.getShowTestimonial());
				testiMonialRepository.save(testiMonial);
				
			}
		});
		model.addAttribute("testi_monials",getTestiMonial());
		return "manage_testimonial";
	}
	
	@RequestMapping(value="deleteTestimonials",method=RequestMethod.GET)
	public String deleteTestimonials(@RequestParam("id") Long id,Model model){
		testiMonialRepository.deleteByTestiMonialId(id);
		model.addAttribute("testi_monials",getTestiMonial());
		return "manage_testimonial";
	}
}
