package com.careervelocity.contoller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.careervelocity.common.PageWrapper;
import com.careervelocity.models.ChangePassword;
import com.careervelocity.models.ErrorMessage;
import com.careervelocity.models.JobSets;
import com.careervelocity.models.TestiMonial;
import com.careervelocity.models.UserProfile;
import com.careervelocity.repository.JobsRepository;
import com.careervelocity.repository.RegistrationRepository;
import com.careervelocity.repository.TestiMonialRepository;

@Controller
public class ChangePasswordController {
	@Autowired
	RegistrationRepository registrationRepository;
	
	@Autowired
	TestiMonialRepository testiMonialRepository;
	
	@Autowired
	JobsRepository jobsRepository;
	
	
	@ModelAttribute("testimonials")
	public List<TestiMonial> getAllTestiMonials(){
		return testiMonialRepository.findAll();
	}
	
	@ModelAttribute("page")
	public PageWrapper<JobSets> getAllJobs(HttpServletRequest request, Pageable pageable){
		PageWrapper<JobSets> page = new PageWrapper<JobSets>(getPageableJobs(request,pageable),"/index");
		return page;
	}
	
	private Page<JobSets> getPageableJobs(HttpServletRequest request,Pageable pageable){
		Page<JobSets> findAll = jobsRepository.findAll(pageable);
		HttpSession session = request.getSession();
		
		String emailId = (String)session.getAttribute("userEmail");
		 Object user = session.getAttribute("user");
		if(emailId != null && user!= null && (boolean)user){
			UserProfile findByUserEmail = registrationRepository.findByUserEmail(emailId);
			if(findByUserEmail != null){
			List<JobSets> appliedJobs = findByUserEmail.getAppliedJobs();
			findAll.forEach(item->{
				if(appliedJobs.contains(item))
				item.setApplied(true);
				else
				item.setApplied(false);
				});
			}
		}
		return findAll;
	}
	
	@ModelAttribute("change_password")
	public ChangePassword getChangePassword(HttpServletRequest request){
		HttpSession session = request.getSession();
		String emailId = (String)session.getAttribute("userEmail");
		ChangePassword changePassword = new ChangePassword();
		if(emailId != null){
			 UserProfile findByUserEmail = registrationRepository.findByUserEmail(emailId);
			 changePassword.setUserId(findByUserEmail.getUserId());
		}
		return changePassword;
	}
	
	@RequestMapping(value="change_password",method=RequestMethod.GET)
	public String getChangePasswordPage(){
		return "change_password";
	}
	
	
	@RequestMapping(value = "change_password", method = RequestMethod.POST)
	public String changePassword(@ModelAttribute ChangePassword changePassword, ModelMap model) {
		UserProfile findById = registrationRepository.findByUserId(changePassword.getUserId());
		try {
			if (findById != null) {
				if (findById.getPassword().equals(changePassword.getOldPassword())) {
					findById.setPassword(changePassword.getNewPassword());
					registrationRepository.save(findById);
					return addErrorMessage(model, "Information", "Updated Successfully", "change_password");
					//return new ModelAndView("redirect:logout", model);
				} else {
					return addErrorMessage(model, "Error", "Updation failed please try after sometime", "change_password");
					//return new ModelAndView("redirect:change_password", model);
				}
				
			}
			return addErrorMessage(model, "Information", "Invalid User Id", "change_password");
		} catch (Exception e) {
			return addErrorMessage(model, "Error", e.getLocalizedMessage(), "change_password");
			//return new ModelAndView("redirect:change_password", model);
		}
	}
	
	private String addErrorMessage(ModelMap model,String title,String message,String page) {
		ErrorMessage errorMessage = new ErrorMessage();
		errorMessage.setMessage(message);
		errorMessage.setTitle(title);
		model.addAttribute("errorMessage", errorMessage);
		return page;
	}
}
