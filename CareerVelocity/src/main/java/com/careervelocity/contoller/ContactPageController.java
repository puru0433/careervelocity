package com.careervelocity.contoller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.careervelocity.common.EmailService;
import com.careervelocity.models.Contact;

@Controller
public class ContactPageController {

	@Value("${emailId: assaypaskal@gmail.com}")
	private String fromEmail;
	@Value("${password: qnfbaxeblxqxcehq}")
	private String password;
	@Value("${toemailId: purushothamaramu@gmail.com}")
	private String toemailId;
	
	
	@RequestMapping(value="/contact",method=RequestMethod.GET)
	public String getContact(){
		return "contact";
	}
	
	@RequestMapping(value="/contactMe",method=RequestMethod.POST)
	public String postContactInfo(@ModelAttribute Contact contact){
		EmailService emailService = new EmailService(fromEmail,password,toemailId,"contact request",contact);
		emailService.start();
		return "contact";
	}
}
