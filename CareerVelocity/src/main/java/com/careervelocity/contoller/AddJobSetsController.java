package com.careervelocity.contoller;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.careervelocity.ApplicationConfig;
import com.careervelocity.common.FileUtility;
import com.careervelocity.common.PageWrapper;
import com.careervelocity.models.ErrorMessage;
import com.careervelocity.models.JobCategory;
import com.careervelocity.models.JobSets;
import com.careervelocity.models.Skills;
import com.careervelocity.models.TestiMonial;
import com.careervelocity.models.UserProfile;
import com.careervelocity.repository.JobCategoryRepository;
import com.careervelocity.repository.JobsRepository;
import com.careervelocity.repository.RegistrationRepository;
import com.careervelocity.repository.SkillsRepository;
import com.careervelocity.repository.TestiMonialRepository;

@Controller
public class AddJobSetsController {
	
	@Autowired
	JobsRepository jobsRepository;
	
	@Autowired
	JobCategoryRepository jobCategoryRepository;
	
	@Autowired
	SkillsRepository skillRepository;
	
	@Autowired
	ApplicationConfig applicationConfig;
	
	@Autowired
	FileUtility fileUtility;
	
	@Autowired
	TestiMonialRepository testiMonialRepository;
	
	@Autowired
	RegistrationRepository registrationRepository;
	
	@ModelAttribute("testimonials")
	public List<TestiMonial> getAllTestiMonials(){
		return testiMonialRepository.findAll();
	}
	
	@ModelAttribute("job_set")
	public JobSets getJobSet(){
		return new JobSets();
	}
	
	@ModelAttribute("job_categories")
	public List<JobCategory> getJobCategories(){
		return jobCategoryRepository.findAll();
	}
	
	@ModelAttribute("skills")
	public List<Skills> getSkills(){
		return skillRepository.findAll();
	}
	
	@ModelAttribute("page")
	public PageWrapper<JobSets> getAllJobs(HttpServletRequest request, Pageable pageable){
		PageWrapper<JobSets> page = new PageWrapper<JobSets>(getPageableJobs(request,pageable),"/index");
		return page;
	}
	
	private Page<JobSets> getPageableJobs(HttpServletRequest request,Pageable pageable){
		Page<JobSets> findAll = jobsRepository.findAll(pageable);
		HttpSession session = request.getSession();
		
		String emailId = (String)session.getAttribute("userEmail");
		 Object user = session.getAttribute("user");
		if(emailId != null && user!= null && (boolean)user){
			UserProfile findByUserEmail = registrationRepository.findByUserEmail(emailId);
			if(findByUserEmail != null){
			List<JobSets> appliedJobs = findByUserEmail.getAppliedJobs();
			findAll.forEach(item->{
				if(appliedJobs.contains(item))
				item.setApplied(true);
				else
				item.setApplied(false);
				});
			}
		}
		return findAll;
	}
	
	@RequestMapping(value="/add-job",method=RequestMethod.GET)
	public String getAddJob(){
		return "add-job";
	}
	
	@RequestMapping(value="register_job", method=RequestMethod.POST)
	public String registerJob(@ModelAttribute JobSets jobSet,Model model){
		try {
			JobSets findById = jobsRepository.findByJobSetId(jobSet.getJobSetId());
			if (findById == null) {
				List<JobCategory> jobCategory = jobSet.getJobCategory();
				jobSet.setJobCategory(setJobCategoryNames(jobCategory));
				jobSet.setSkillSet(setSkills(jobSet.getSkillSet()));
				addFilepath(jobSet);
				jobsRepository.save(jobSet);
			} else {
				findById.setAddress(jobSet.getAddress());
				findById.setCompanyName(jobSet.getCompanyName());
				findById.setEmail(jobSet.getEmail());
				findById.setMinExperience(jobSet.getMinExperience());
				findById.setMaxExperience(jobSet.getMaxExperience());
				findById.setJobCategory(setJobCategoryNames(jobSet.getJobCategory()));
				findById.setJobDescription(jobSet.getJobDescription());
				findById.setJobTitle(jobSet.getJobTitle());
				findById.setLogoPath(jobSet.getLogoPath());
				findById.setPhoneNumber(jobSet.getPhoneNumber());
				findById.setWebSiteAddress(jobSet.getWebSiteAddress());
				addFilepath(findById);
				jobsRepository.save(findById);
			}
			return addErrorMessage(model,"Information","Added Job Successfully","add-job");
		} catch (Exception e) {
			return addErrorMessage(model,"Error",e.getLocalizedMessage(),"add-job");
		}
	}

	private void addFilepath(JobSets jobSet) {
		String localPath = applicationConfig.getBucketName() + "/resumes/";
		String filePath = System.getProperty("user.home") + File.separator + localPath;
		String logoPath = fileUtility.multiPartFileValidateAndDeleteRewrite(filePath, jobSet.getLogo(),
				filePath, jobSet.getLogoPath(), localPath);
		jobSet.setLogoPath(logoPath);
	}
	
	

	@RequestMapping(value="edit_job",method=RequestMethod.POST)
	public String editJob(@ModelAttribute JobSets jobSet,Model model){
		model.addAttribute("job_set",jobsRepository.findByJobSetId(jobSet.getJobSetId()));
		return "add-job";
	}

	private List<Skills> setSkills(List<Skills> skillSet) {
		List<Skills>  skills = new ArrayList<Skills>();
		skillSet.forEach(item->{
			skills.add(skillRepository.findBySkillsId(item.getSkillsId()));
		});
		return skills;
	}

	private List<JobCategory> setJobCategoryNames(List<JobCategory> jobCategory) {
		List<JobCategory> jobCategoryList = new ArrayList<JobCategory>();
		jobCategory.forEach(item->{
			jobCategoryList.add(jobCategoryRepository.findByJobCategoryId(item.getJobCategoryId()));
		});
		return jobCategoryList;
	}
	
	private String addErrorMessage(Model model,String title,String message,String page) {
		ErrorMessage errorMessage = new ErrorMessage();
		errorMessage.setMessage(message);
		errorMessage.setTitle(title);
		model.addAttribute("errorMessage", errorMessage);
		return page;
	}
	//added comment
}
