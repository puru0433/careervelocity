package com.careervelocity.contoller;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.careervelocity.ApplicationConfig;
import com.careervelocity.common.FileUtility;
import com.careervelocity.common.PageWrapper;
import com.careervelocity.models.Education;
import com.careervelocity.models.ErrorMessage;
import com.careervelocity.models.Experience;
import com.careervelocity.models.Graduation;
import com.careervelocity.models.JobSets;
import com.careervelocity.models.TestiMonial;
import com.careervelocity.models.University;
import com.careervelocity.models.UserProfile;
import com.careervelocity.repository.EducationRepository;
import com.careervelocity.repository.ExperienceRepository;
import com.careervelocity.repository.GraduationRepository;
import com.careervelocity.repository.JobsRepository;
import com.careervelocity.repository.RegistrationRepository;
import com.careervelocity.repository.TestiMonialRepository;
import com.careervelocity.repository.UniversityRepository;

@Controller
public class RegistrationController {

	
	@Autowired
	RegistrationRepository registrationRepository;
	
	@Autowired
	JobsRepository jobsRepository;
	
	@Autowired
	ApplicationConfig applicationConfig;
	
	@Autowired
	FileUtility fileUtility;
	
	@Autowired
	GraduationRepository graduationRepository;
	
	@Autowired
	UniversityRepository universityRepository;
	
	@Autowired
	EducationRepository educationRepository;
	
	@Autowired
	ExperienceRepository experienceRepository;
	
	@Autowired
	TestiMonialRepository testiMonialRepository;
	
	
	@ModelAttribute("testimonials")
	public List<TestiMonial> getAllTestiMonials(){
		return testiMonialRepository.findAll();
	}
	
	@ModelAttribute("page")
	public PageWrapper<JobSets> getAllJobs(HttpServletRequest request, Pageable pageable){
		PageWrapper<JobSets> page = new PageWrapper<JobSets>(getPageableJobs(request,pageable),"/index");
		return page;
	}
	
	private Page<JobSets> getPageableJobs(HttpServletRequest request,Pageable pageable){
		Page<JobSets> findAll = jobsRepository.findAll(pageable);
		HttpSession session = request.getSession();
		
		String emailId = (String)session.getAttribute("userEmail");
		 Object user = session.getAttribute("user");
		if(emailId != null && user!= null && (boolean)user){
			UserProfile findByUserEmail = registrationRepository.findByUserEmail(emailId);
			if(findByUserEmail != null){
			List<JobSets> appliedJobs = findByUserEmail.getAppliedJobs();
			findAll.forEach(item->{
				if(appliedJobs.contains(item))
				item.setApplied(true);
				else
				item.setApplied(false);
				});
			}
		}
		return findAll;
	}
	
	@ModelAttribute("all_jobs")
	public List<JobSets> getAllJobs(HttpServletRequest request){
		List<JobSets> findAll = jobsRepository.findAll();
		HttpSession session = request.getSession();
		
		String emailId = (String)session.getAttribute("userEmail");
		 Object user = session.getAttribute("user");
		if(emailId != null && user!= null && (boolean)user){
			UserProfile findByUserEmail = registrationRepository.findByUserEmail(emailId);
			if(findByUserEmail != null){
			List<JobSets> appliedJobs = findByUserEmail.getAppliedJobs();
			findAll.forEach(item->{
				if(appliedJobs.contains(item))
				item.setApplied(true);
				else
				item.setApplied(false);
				});
			}
		}
		return findAll;
	}
	
	@ModelAttribute("user_profile")
	public UserProfile getUserProfile(HttpServletRequest request){
		HttpSession session = request.getSession();
		String emailId = (String)session.getAttribute("userEmail");
		if(emailId != null){
			return registrationRepository.findByUserEmail(emailId);
		}
		UserProfile userProfile = new UserProfile();
		Education education = new Education();
		Graduation graduation = new Graduation();
		graduation.setGraduationName("BE");
		education.setCourseType("FT");
		education.setGraduation(graduation);
		University university = new University();
		university.setUniversityName("VTU");
		education.setPercentage(50.05d);
		education.setUniversity(university);
		education.setYear("2011");
		List<Education> educationList= new ArrayList<Education>();
		educationList.add(education);
		userProfile.setEducation(educationList);
		return userProfile;
	}
	
	@ModelAttribute("sample_education")
	public Education getSampleEducation(){
		return new Education();
	}
	
	@ModelAttribute("graduation_list")
	public List<Graduation> getGraduationList(HttpServletRequest request){
		return graduationRepository.findAll();
	}
	
	@ModelAttribute("university_list")
	public List<University> getUniversityList(HttpServletRequest request){
		return universityRepository.findAll();
	}
	
	@RequestMapping(value="/signup",method=RequestMethod.GET)
	public String getSignUp(){
		System.out.println("Index called");
		return "signup";
	}
	
	@RequestMapping(value="register",method=RequestMethod.POST)
	public String register(@ModelAttribute UserProfile userProfile,Model model){
		
		UserProfile findByUserEmail = registrationRepository.findByUserEmail(userProfile.getUserEmail());
		if(findByUserEmail == null){
			registrationRepository.save(userProfile);
			return addErrorMessage( model, "Information", "Registered Successfully","index");
		}else{
			model.addAttribute("userProfileTest",userProfile);
			return addErrorMessage( model, "Information", "User With Same Email Already Exist","signup");
		}
		
	}

	@RequestMapping(value = "update_profile", method = RequestMethod.POST)
	public String updateProfile(@ModelAttribute UserProfile userProfile, Model model) {
		UserProfile findByUserEmail = registrationRepository.findByUserId(userProfile.getUserId());
		try {
			if (findByUserEmail != null) {
				findByUserEmail.setPhoneNo(userProfile.getPhoneNo());
				findByUserEmail.setUserEmail(userProfile.getUserEmail());
				findByUserEmail.setUserFirstName(userProfile.getUserFirstName());
				findByUserEmail.setUserLastName(userProfile.getUserLastName());
				findByUserEmail.setUserName(userProfile.getUserName());
				findByUserEmail.setEducation(userProfile.getEducation());
				registrationRepository.save(findByUserEmail);
				return addErrorMessage(model, "Update", "Updated Successfully", "edit_profile");
			} else {
				return addErrorMessage(model, "Error", "Error Occurred Please Tyr Later", "edit_profile");
			}
		} catch (Exception e) {
			return addErrorMessage(model, "Update", e.getLocalizedMessage(), "edit_profile");
		}
	}
	
	@RequestMapping(value = "edit_profile",method=RequestMethod.GET)
	public String getEditProfilePage(HttpServletRequest request, Model model){
		HttpSession session = request.getSession();
		String email= (String)session.getAttribute("userEmail");
		UserProfile findByUserEmail = registrationRepository.findByUserEmail(email);
		try {
			if (findByUserEmail != null) {
			 model.addAttribute("user_profile",findByUserEmail);
			 model.addAttribute("empty_eduation",new Education());
			 model.addAttribute("graduation_list", getGraduationList(request));
			 return "edit_profile";
			} else {
				return addErrorMessage(model, "Error", "Error Occurred Please Tyr Later", "edit_profile");
			}
		} catch (Exception e) {
			return addErrorMessage(model, "Update", e.getLocalizedMessage(), "edit_profile");
		}
	}
	
	@RequestMapping(value ="upload_resume",method=RequestMethod.GET)
	public String getUploadResumePage(){
		return "upload_resume";
	}
	
	

	@RequestMapping(value="update_personal_profile",method=RequestMethod.POST)
	public String addPersonalInformation(@ModelAttribute UserProfile userProfile,Model model){
		UserProfile findById = registrationRepository.findByUserId(userProfile.getUserId());
		findById.setUserFirstName(userProfile.getUserFirstName());
		findById.setUserLastName(userProfile.getUserLastName());
		findById.setUserEmail(userProfile.getUserEmail());
		findById.setUserName(userProfile.getUserName());
		findById.setPhoneNo(userProfile.getPhoneNo());
		UserProfile savedprofile = registrationRepository.save(findById);
		model.addAttribute("user_profile",savedprofile);
		return "edit_profile";
	}
	
	@RequestMapping(value ="add_education",method=RequestMethod.POST)
	public String addEducation(@ModelAttribute Education education,@ModelAttribute("user_profile") UserProfile userProfile,Model model){
		education.setGraduation(graduationRepository.findByGraduationId(education.getGraduation().getGraduationId()));
		education.setUniversity(universityRepository.findByUniverSityId(education.getUniversity().getUniverSityId()));
		Education savedEducation = educationRepository.save(education);
		userProfile.getEducation().add(savedEducation);
		UserProfile savedprofile = registrationRepository.save(userProfile);
		model.addAttribute("user_profile",savedprofile);
		return "edit_profile";
	}
	
	@RequestMapping(value="update_education",method=RequestMethod.POST)
	public String updateEducation (@ModelAttribute Education education,@ModelAttribute("user_profile") UserProfile userProfile,Model model){
		educationRepository.save(education);
		UserProfile savedprofile = registrationRepository.save(userProfile);
		model.addAttribute("user_profile",savedprofile);
		return "edit_profile";
		
	}
	
	@RequestMapping(value="delete_education",method=RequestMethod.POST)
	public String deleteEducation (@ModelAttribute Education education,@ModelAttribute("user_profile") UserProfile userProfile,Model model){
		Education findByEducationId = educationRepository.findByEducationId(education.getEducationId());
		UserProfile savedprofile = registrationRepository.findByUserId(userProfile.getUserId());
		savedprofile.getEducation().remove(findByEducationId);
		UserProfile save = registrationRepository.save(savedprofile);
		model.addAttribute("user_profile",save);
		return "edit_profile";
		
	}
	
	@RequestMapping(value ="add_experience",method=RequestMethod.POST)
	public String addExperience(@ModelAttribute Experience experience,@ModelAttribute("user_profile") UserProfile userProfile,Model model){
		try {
			Date date1=new SimpleDateFormat("yyyy-MM-dd").parse(experience.getJoiningDate());
			Date date2=new SimpleDateFormat("yyyy-MM-dd").parse(experience.getEndDate());
			experience.setStartEpocTime(date1.getTime());
			experience.setEndEpocTime(date2.getTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<Experience> experience2 = userProfile.getExperience();
		if(checkValidExperience(experience2,experience)){
			Experience savedExperience = experienceRepository.save(experience);
			experience2.add(savedExperience);
			savedExperience.setJoiningDate(experience.getJoiningDate());
			savedExperience.setTypeOfEmployer(experience.getTypeOfEmployer());
			UserProfile findById = registrationRepository.findByUserId(userProfile.getUserId());
			if(findById.getExperience().size()>1){
				findById.setTypeOfExperience("experienced");
				registrationRepository.save(findById);
				model.addAttribute("user_profile",findById);
				
			}else{
				findById.setTypeOfExperience("Fresher");
				registrationRepository.save(findById);
				model.addAttribute("user_profile",findById);
			}
		}else{
			addErrorMessage(model, "Warning", "Invalid Experience Date", "edit_profile");
		}
		if(experience2.size()>1){
			userProfile.setTypeOfExperience("experienced");
		}
		
		return "edit_profile";
	}
	
	private boolean checkValidExperience(List<Experience> experience2, Experience experience) {
		long lastEndTime=0;
		for(Experience experienceItem:experience2){
			long endEpocTime = experienceItem.getEndEpocTime();
			if(lastEndTime<endEpocTime){
				lastEndTime=endEpocTime;
			}
		}
		if(experience.getEndEpocTime()>lastEndTime){
			return true;
		}else{
		return false;
		}
	}

	@RequestMapping(value="delete_experience",method=RequestMethod.POST)
	public String deleteExperience(@ModelAttribute Experience experience,@ModelAttribute("user_profile") UserProfile userProfile,Model model){
	try{
		Experience findOne = experienceRepository.findOne(experience.getExperienceId());
	UserProfile findById = registrationRepository.findByUserId(userProfile.getUserId());
	findById.getExperience().remove(findOne);
	UserProfile save = registrationRepository.save(findById);
	model.addAttribute("user_profile",save);
	}catch(Exception e){
		e.printStackTrace();
	}
	return "edit_profile";
	}
	
	@RequestMapping(value="updateExperience",method=RequestMethod.POST)
	public String updatedExperience(@ModelAttribute Experience experience,@ModelAttribute("user_profile") UserProfile userProfile,Model model){
		Experience findOne = experienceRepository.findOne(experience.getExperienceId());
		findOne.setDesignation(experience.getDesignation());
		findOne.setEmployerName(experience.getEmployerName());
		findOne.setEndDate(experience.getEndDate());
		try {
			Date date1=new SimpleDateFormat("dd-MM-yyyy").parse(experience.getJoiningDate());
			Date date2=new SimpleDateFormat("dd-MM-yyyy").parse(experience.getEndDate());
			findOne.setStartEpocTime(date1.getTime());
			findOne.setEndEpocTime(date2.getTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<Experience> experience2 = userProfile.getExperience();
		if(checkValidExperience(experience2,experience)){
			findOne.setJoiningDate(experience.getJoiningDate());
			findOne.setTypeOfEmployer(experience.getTypeOfEmployer());
			experienceRepository.save(findOne);
		}else{
			addErrorMessage(model, "Warning", "Invalid Experience Date", "edit_profile");
		}
		if(experience2.size()>1){
			UserProfile findById = registrationRepository.findByUserId(userProfile.getUserId());
			findById.setTypeOfExperience("experienced");
			registrationRepository.save(findById);
			model.addAttribute("user_profile",findById);
			
		}else{
			UserProfile findById = registrationRepository.findByUserId(userProfile.getUserId());
			findById.setTypeOfExperience("Fresher");
			registrationRepository.save(findById);
			model.addAttribute("user_profile",findById);
		}
		
		return "edit_profile";
	}
	
	@RequestMapping(value ="upload_resume",method=RequestMethod.POST)
	public String getUploadResumePage(@ModelAttribute UserProfile userProfile,Model model){
		try {
			String localPath = applicationConfig.getBucketName() + "/resumes/";
			String filePath = System.getProperty("user.home") + File.separator + localPath;
			String resumePath = fileUtility.multiPartFileValidateAndDeleteRewrite(filePath, userProfile.getResume(),
					filePath, userProfile.getResumePath(), localPath);
			UserProfile findByUserEmail = registrationRepository.findByUserId(userProfile.getUserId());
			findByUserEmail.setResumePath(resumePath);
			UserProfile savedUserProfile = registrationRepository.save(findByUserEmail);
			model.addAttribute("user_profile", savedUserProfile);
			return addErrorMessage(model, "Information", "Successfully Uploaded", "edit_profile");
		} catch (Exception e) {
			return addErrorMessage(model, "Information", e.getLocalizedMessage(), "edit_profile");
		}
	}
	
	
	
	private String addErrorMessage(Model model,String title,String message,String page) {
		ErrorMessage errorMessage = new ErrorMessage();
		errorMessage.setTitle(title);
		errorMessage.setMessage(message);
		model.addAttribute("errorMessage", errorMessage);
		return page;
	}
}
