package com.careervelocity.common;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;


public class LoggerUtility {

	public static LoggerUtility instance;
	public Logger logger;
	
	public static LoggerUtility getInstance(){
		synchronized (LoggerUtility.class) {
			if(instance == null){
				instance = new LoggerUtility();
			}
			
		}
		return instance;
	}
	
	private LoggerUtility(){
		String rootPath = System.getProperty("user.home"); 
		String filePath ="myLog";
		 SimpleDateFormat format = new SimpleDateFormat("M-d_HHmmss");
		 String folderPath = rootPath + File.separator+"cvrepo";
		if (rootPath != null) {
			// filePath = rootPath + File.separator + "AssayFileRepo"+File.separator+"logs"+File.separator+"institute"+format.format(Calendar.getInstance().getTime())+".log";
			
			filePath =folderPath+File.separator+"cv.log";
		}
		 File f = new File(folderPath);
		 if(!f.exists()){
			 f.mkdir();
		 }
		 f= new File(filePath);
		 try {
			f.createNewFile();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	     logger = Logger.getLogger("cv");  
	     System.out.println("LOGGER CREATED IN "+filePath);
	    FileHandler fh;  

	    try {  

	        // This block configure the logger with handler and formatter  
	        fh = new FileHandler(filePath);  
	        logger.addHandler(fh);
	        SimpleFormatter formatter = new SimpleFormatter();  
	        fh.setFormatter(formatter);  

	        // the following statement is used to log any messages  

	    } catch (SecurityException e) {  
	        e.printStackTrace();  
	    } catch (IOException e) {  
	        e.printStackTrace();  
	    }  


	}
}
