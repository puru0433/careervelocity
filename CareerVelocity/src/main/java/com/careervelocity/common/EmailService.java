package com.careervelocity.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.careervelocity.models.Contact;

public class EmailService extends Thread {

	String fromEmailId;

	String fromEmailPassword;

	String toEmailId;
	
	String subject;
	
	String message;

	static Properties mailServerProperties;
	
	static Session getMailSession;
	
	static MimeMessage generateMailMessage;



	public EmailService(String emailId, String adminPassword, String userEmailId, 
			String subject2, Contact contact) {
		this.fromEmailId = emailId;
		ArrayList<String> tempEmailId = new ArrayList<String>();
		tempEmailId.add(userEmailId);
		this.toEmailId = userEmailId;
		this.fromEmailPassword = adminPassword;
		this.message = subject2;
		this.subject = contact.getEmail()+"\n"+contact.getName()+"\n"+contact.getSubject()+"\n"+contact.getMessage();
		try{
		System.out.println("\n 1st ===> setup Mail Server Properties..");
		mailServerProperties = System.getProperties();
		mailServerProperties.put("mail.smtp.port", "587");
		mailServerProperties.put("mail.smtp.auth", "false");
		mailServerProperties.put("mail.smtp.starttls.enable", "true");
		System.out.println("Mail Server Properties have been setup successfully..");

		// Step2
		System.out.println("\n\n 2nd ===> get Mail Session..");
		getMailSession = Session.getDefaultInstance(mailServerProperties, null);
		generateMailMessage = new MimeMessage(getMailSession);
		
			generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(userEmailId));
			generateMailMessage.setContent(subject2 +"\n your temp pwd "+ adminPassword, "text/html");
		
		generateMailMessage.setSubject(subject2);
		
		System.out.println("Mail Session has been created successfully..");

		// Step3
		System.out.println("\n\n 3rd ===> Get Session and Send mail");
		Transport transport = getMailSession.getTransport("smtp");

		// Enter your correct gmail UserID and Password
		// if you have 2FA enabled then provide App Specific Password
		System.out.println();
		transport.connect("smtp.gmail.com", fromEmailId, fromEmailPassword);
		transport.sendMessage(generateMailMessage, generateMailMessage.getAllRecipients());
		transport.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void generateAndSendEmail() throws AddressException, MessagingException {

		// Step1
		System.out.println("\n 1st ===> setup Mail Server Properties..");
		mailServerProperties = System.getProperties();
		mailServerProperties.put("mail.smtp.port", "587");
		mailServerProperties.put("mail.smtp.auth", "false");
		mailServerProperties.put("mail.smtp.starttls.enable", "true");
		System.out.println("Mail Server Properties have been setup successfully..");

		// Step2
		System.out.println("\n\n 2nd ===> get Mail Session..");
		getMailSession = Session.getDefaultInstance(mailServerProperties, null);
		generateMailMessage = new MimeMessage(getMailSession);
		
			generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(this.toEmailId));
			String emailBody = this.message;
			generateMailMessage.setContent(emailBody, "text/html");
		
		generateMailMessage.setSubject(subject);
		
		System.out.println("Mail Session has been created successfully..");

		// Step3
		System.out.println("\n\n 3rd ===> Get Session and Send mail");
		Transport transport = getMailSession.getTransport("smtp");

		// Enter your correct gmail UserID and Password
		// if you have 2FA enabled then provide App Specific Password
		System.out.println();
		transport.connect("smtp.gmail.com", fromEmailId, fromEmailPassword);
		transport.sendMessage(generateMailMessage, generateMailMessage.getAllRecipients());
		transport.close();
		
	}

	public void run() {
		try {
			generateAndSendEmail();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
