package com.careervelocity.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Experience")
public class Experience {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long experienceId;
	private String employerName;
	private String joiningDate;
	private String endDate;
	private String designation;
	private String typeOfEmployer;
	private long startEpocTime;
	private long endEpocTime;
	
	public Long getExperienceId() {
		return experienceId;
	}
	public void setExperienceId(Long experienceId) {
		this.experienceId = experienceId;
	}
	public String getEmployerName() {
		return employerName;
	}
	public void setEmployerName(String employerName) {
		this.employerName = employerName;
	}
	public String getJoiningDate() {
		return joiningDate;
	}
	public void setJoiningDate(String joiningDate) {
		this.joiningDate = joiningDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getTypeOfEmployer() {
		return typeOfEmployer;
	}
	public void setTypeOfEmployer(String typeOfEmployer) {
		this.typeOfEmployer = typeOfEmployer;
	}
	public long getStartEpocTime() {
		return startEpocTime;
	}
	public void setStartEpocTime(long startEpocTime) {
		this.startEpocTime = startEpocTime;
	}
	public long getEndEpocTime() {
		return endEpocTime;
	}
	public void setEndEpocTime(long endEpocTime) {
		this.endEpocTime = endEpocTime;
	}
	@Override
	public String toString() {
		return "Experience [id=" + experienceId + ", employerName=" + employerName + ", joiningDate=" + joiningDate + ", endDate="
				+ endDate + ", designation=" + designation + ", typeOfEmployer=" + typeOfEmployer + "]";
	}
	
}
