package com.careervelocity.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="University")
public class University {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long univerSityId;
	private String universityName;
	
	public Long getUniverSityId() {
		return univerSityId;
	}
	public void setUniverSityId(Long univerSityId) {
		this.univerSityId = univerSityId;
	}
	public String getUniversityName() {
		return universityName;
	}
	public void setUniversityName(String universityName) {
		this.universityName = universityName;
	}
	
	
}
