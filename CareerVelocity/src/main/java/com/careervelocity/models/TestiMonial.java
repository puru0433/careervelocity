package com.careervelocity.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="TestiMonial")
public class TestiMonial {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long testiMonialId;
	private String userName;
	private String message;
	private Boolean showTestimonial;
	
	@Transient
	private boolean selected;
	
	public Long getTestiMonialId() {
		return testiMonialId;
	}
	public void setTestiMonialId(Long testiMonialId) {
		this.testiMonialId = testiMonialId;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Boolean getShowTestimonial() {
		return showTestimonial;
	}
	public void setShowTestimonial(Boolean showTestimonial) {
		this.showTestimonial = showTestimonial;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	
}
