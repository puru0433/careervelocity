package com.careervelocity.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Services")
public class Services {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long serviceId;
	private String serviceName;
	private String serviceDesc;
	private double amt;
	private Boolean isActive;
	private String startDate;
	private String endDate;
	private long startEpoc;
	private long endEpoc;
	private Boolean isChoosed;
	public long getServiceId() {
		return serviceId;
	}
	public void setServiceId(long serviceId) {
		this.serviceId = serviceId;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getServiceDesc() {
		return serviceDesc;
	}
	public void setServiceDesc(String serviceDesc) {
		this.serviceDesc = serviceDesc;
	}
	public double getAmt() {
		return amt;
	}
	public void setAmt(double amt) {
		this.amt = amt;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public long getStartEpoc() {
		return startEpoc;
	}
	public void setStartEpoc(long startEpoc) {
		this.startEpoc = startEpoc;
	}
	public long getEndEpoc() {
		return endEpoc;
	}
	public void setEndEpoc(long endEpoc) {
		this.endEpoc = endEpoc;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public Boolean getIsChoosed() {
		return isChoosed;
	}
	public void setIsChoosed(Boolean isChoosed) {
		this.isChoosed = isChoosed;
	}
	
	
}
