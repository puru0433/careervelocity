package com.careervelocity.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Graduation")
public class Graduation {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long graduationId;
	
	private String graduationName;


	public Long getGraduationId() {
		return graduationId;
	}

	public void setGraduationId(Long graduationId) {
		this.graduationId = graduationId;
	}

	public String getGraduationName() {
		return graduationName;
	}

	public void setGraduationName(String graduationName) {
		this.graduationName = graduationName;
	}

	@Override
	public String toString() {
		return "Graduation [id=" + graduationId + ", graduationName=" + graduationName + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((graduationName == null) ? 0 : graduationName.hashCode());
		result = prime * result + ((graduationId == null) ? 0 : graduationId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Graduation other = (Graduation) obj;
		if (graduationName == null) {
			if (other.graduationName != null)
				return false;
		} else if (!graduationName.equals(other.graduationName))
			return false;
		if (graduationId == null) {
			if (other.graduationId != null)
				return false;
		} else if (!graduationId.equals(other.graduationId))
			return false;
		return true;
	}
	
	
}
