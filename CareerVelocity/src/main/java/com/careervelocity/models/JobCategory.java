package com.careervelocity.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="JobCategory")
public class JobCategory {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long jobCategoryId;
	private String category;
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((jobCategoryId == null) ? 0 : jobCategoryId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JobCategory other = (JobCategory) obj;
		if (jobCategoryId == null) {
			if (other.jobCategoryId != null)
				return false;
		} else if (!jobCategoryId.equals(other.jobCategoryId))
			return false;
		return true;
	}
	
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Long getJobCategoryId() {
		return jobCategoryId;
	}
	public void setJobCategoryId(Long jobCategoryId) {
		this.jobCategoryId = jobCategoryId;
	}
	
}
