package com.careervelocity.models.wrappers;

import java.util.List;

import com.careervelocity.models.TestiMonial;

public class TestiMonailWrapper {
	private List<TestiMonial> testiMonialList;

	public List<TestiMonial> getTestiMonialList() {
		return testiMonialList;
	}

	public void setTestiMonialList(List<TestiMonial> testiMonialList) {
		this.testiMonialList = testiMonialList;
	}
}
