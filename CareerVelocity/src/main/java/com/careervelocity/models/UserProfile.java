package com.careervelocity.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.springframework.web.multipart.MultipartFile;

@Entity
@Table(name="UserProfile")
public class UserProfile {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long userId;
	private String userEmail;
	private String userFirstName;
	private String userLastName;
	private String userName;
	private String password;
	private String cPassword;
	private String courseType;
	private String resumePath;
	
	
	@Transient
	private MultipartFile resume;
	@OneToMany
	private List<JobSets> appliedJobs;
	@OneToMany
	private List<Education> education = new ArrayList<Education>();
	@OneToMany
	private List<Experience> experience = new ArrayList<Experience>();
	@OneToMany
	private List<Services> services = new ArrayList<Services>();
	private String phoneNo;
	private String typeOfExperience;
	
	public String getcPassword() {
		return cPassword;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public void setcPassword(String cPassword) {
		this.cPassword = cPassword;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getUserFirstName() {
		return userFirstName;
	}
	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}
	public String getUserLastName() {
		return userLastName;
	}
	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public List<JobSets> getAppliedJobs() {
		return appliedJobs;
	}
	public void setAppliedJobs(List<JobSets> appliedJobs) {
		this.appliedJobs = appliedJobs;
	}
	public String getResumePath() {
		return resumePath;
	}
	public void setResumePath(String resumePath) {
		this.resumePath = resumePath;
	}
	public MultipartFile getResume() {
		return resume;
	}
	public void setResume(MultipartFile resume) {
		this.resume = resume;
	}
	public String getCourseType() {
		return courseType;
	}
	public void setCourseType(String courseType) {
		this.courseType = courseType;
	}
	public List<Education> getEducation() {
		return education;
	}
	public void setEducation(List<Education> education) {
		this.education = education;
	}
	public List<Experience> getExperience() {
		return experience;
	}
	public List<Services> getServices() {
		return services;
	}
	public void setServices(List<Services> services) {
		this.services = services;
	}
	public void setExperience(List<Experience> experience) {
		this.experience = experience;
	}
	public String getTypeOfExperience() {
		return typeOfExperience;
	}
	public void setTypeOfExperience(String typeOfExperience) {
		this.typeOfExperience = typeOfExperience;
	}
	@Override
	public String toString() {
		return "UserProfile [id=" + userId + ", userEmail=" + userEmail + ", userFirstName=" + userFirstName
				+ ", userLastName=" + userLastName + ", userName=" + userName + ", password=" + password
				+ ", cPassword=" + cPassword + ", appliedJobs=" + appliedJobs + ", phoneNo=" + phoneNo + "]";
	}
	
	
}
