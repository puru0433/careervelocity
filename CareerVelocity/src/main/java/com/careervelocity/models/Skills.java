package com.careervelocity.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Skills")
public class Skills {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long skillsId;
	private String skillName;
	
	
	public Long getSkillsId() {
		return skillsId;
	}
	public void setSkillsId(Long skillsId) {
		this.skillsId = skillsId;
	}
	public String getSkillName() {
		return skillName;
	}
	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((skillsId == null) ? 0 : skillsId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Skills other = (Skills) obj;
		if (skillsId == null) {
			if (other.skillsId!= null)
				return false;
		} else if (!skillsId.equals(other.skillsId))
			return false;
		return true;
	}
	
	
}
