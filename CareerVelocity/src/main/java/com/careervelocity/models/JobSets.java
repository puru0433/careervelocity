package com.careervelocity.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.springframework.web.multipart.MultipartFile;
@Entity
@Table(name="JobSets")
public class JobSets {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long jobSetId;
	public Long getJobSetId() {
		return jobSetId;
	}
	public void setJobSetId(Long jobSetId) {
		this.jobSetId = jobSetId;
	}
	private String jobTitle;
	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE,
        org.hibernate.annotations.CascadeType.DELETE,
        org.hibernate.annotations.CascadeType.MERGE,
        org.hibernate.annotations.CascadeType.PERSIST
       })
	private List<Skills> skillSet;
	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE,
        org.hibernate.annotations.CascadeType.DELETE,
        org.hibernate.annotations.CascadeType.MERGE,
        org.hibernate.annotations.CascadeType.PERSIST
       })
	private List<JobCategory> jobCategory;
	@Column(length = Integer.MAX_VALUE)
	private String jobDescription;
	private String minExperience;
	private String maxExperience;
	private String companyName;
	private String phoneNumber;
	private String email;
	private String webSiteAddress;
	@Column(length = Integer.MAX_VALUE)
	private String address;
	private String logoPath;
	@Transient
	private MultipartFile logo;
	
	@Transient
	private boolean applied;
	
	public String getJobTitle() {
		return jobTitle;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	public List<Skills> getSkillSet() {
		return skillSet;
	}
	public void setSkillSet(List<Skills> skillSet) {
		this.skillSet = skillSet;
	}
	public List<JobCategory> getJobCategory() {
		return jobCategory;
	}
	public void setJobCategory(List<JobCategory> jobCategory) {
		this.jobCategory = jobCategory;
	}
	public String getJobDescription() {
		return jobDescription;
	}
	public void setJobDescription(String jobDescription) {
		this.jobDescription = jobDescription;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getWebSiteAddress() {
		return webSiteAddress;
	}
	public void setWebSiteAddress(String webSiteAddress) {
		this.webSiteAddress = webSiteAddress;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getLogoPath() {
		return logoPath;
	}
	public void setLogoPath(String logoPath) {
		this.logoPath = logoPath;
	}
	public MultipartFile getLogo() {
		return logo;
	}
	public void setLogo(MultipartFile logo) {
		this.logo = logo;
	}
	public boolean isApplied() {
		return applied;
	}
	public void setApplied(boolean applied) {
		this.applied = applied;
	}
	public String getMinExperience() {
		return minExperience;
	}
	public void setMinExperience(String minExperience) {
		this.minExperience = minExperience;
	}
	public String getMaxExperience() {
		return maxExperience;
	}
	public void setMaxExperience(String maxExperience) {
		this.maxExperience = maxExperience;
	}
	
	
}
