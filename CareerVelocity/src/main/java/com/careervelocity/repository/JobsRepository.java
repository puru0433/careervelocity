package com.careervelocity.repository;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.careervelocity.models.JobSets;

public interface JobsRepository extends JpaRepository<JobSets, Long>{

	@Transactional
	void deleteByJobSetId(Long id);

	JobSets findByJobSetId(Long id);

	 public  Page<JobSets> findAll(Pageable request);
}
