package com.careervelocity.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.careervelocity.models.TestiMonial;

public interface TestiMonialRepository extends JpaRepository<TestiMonial, Long>{

	TestiMonial findByTestiMonialId(Long testiMonialId);

	@Transactional
	void deleteByTestiMonialId(Long id);

	
}
