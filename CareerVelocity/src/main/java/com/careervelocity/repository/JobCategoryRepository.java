package com.careervelocity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.careervelocity.models.JobCategory;

public interface JobCategoryRepository extends JpaRepository<JobCategory, Long>{

	JobCategory findByJobCategoryId(Long jobCategoryId);

}
