package com.careervelocity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.careervelocity.models.UserProfile;
import java.util.List;

public interface RegistrationRepository extends JpaRepository<UserProfile,Long>{
		UserProfile findByUserEmail(String useremail);

		UserProfile findByUserEmailAndPassword(String userEmail, String password);

		UserProfile findByUserId(Long userId);
		
		List<UserProfile> findByAppliedJobs(List appliedjobs);
		
}
