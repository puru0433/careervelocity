package com.careervelocity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.careervelocity.models.University;

public interface UniversityRepository extends JpaRepository<University,Long>{

	University findByUniverSityId(Long univerSityId);

}
