package com.careervelocity.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.careervelocity.models.Education;

public interface EducationRepository extends JpaRepository<Education,Long>{

	Education findByEducationId(Long educationId);

	@Transactional
	void deleteByEducationId(Long educationId);

}
