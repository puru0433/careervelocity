package com.careervelocity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.careervelocity.models.Skills;

public interface SkillsRepository extends JpaRepository<Skills,Long>{

	Skills findBySkillsId(Long skillsId);

}
