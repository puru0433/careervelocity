package com.careervelocity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.careervelocity.models.Graduation;
import java.lang.Long;
import java.util.List;

public interface GraduationRepository extends JpaRepository<Graduation,Long>{

	Graduation findByGraduationId(Long graduationId);
}
