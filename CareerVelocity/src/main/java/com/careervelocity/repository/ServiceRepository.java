package com.careervelocity.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.careervelocity.models.Services;

public interface ServiceRepository extends JpaRepository<Services,Long>{

	@Transactional
	void deleteByServiceId(long serviceId);

	List<Services> findByServiceIdIn(List<Long> serviceId);
}
