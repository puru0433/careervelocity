package com.careervelocity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.careervelocity.models.Experience;

public interface ExperienceRepository extends JpaRepository<Experience,Long>{

}
