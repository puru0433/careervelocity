package com.careervelocity.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.format.Formatter;

import com.careervelocity.models.Graduation;


public class GraduationFormatter implements Formatter<Graduation>{

	@Override
	public String print(Graduation object, Locale locale) {
		return object.getGraduationId()+"";
	}

	@Override
	public Graduation parse(String text, Locale locale) throws ParseException {
		Graduation graduation = new Graduation();
		graduation.setGraduationId(Long.parseLong(text));
		return graduation;
	}

}
