package com.careervelocity.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.format.Formatter;

import com.careervelocity.models.JobCategory;


public class JobCategoryFormatter implements Formatter<JobCategory>{

	@Override
	public String print(JobCategory jobCategory, Locale arg1) {
		return jobCategory.getJobCategoryId()+"";
	}

	@Override
	public JobCategory parse(String text, Locale arg1) throws ParseException {
		JobCategory jobCategory = new JobCategory();
		jobCategory.setJobCategoryId(Long.parseLong(text));
		return jobCategory;
	}

}
