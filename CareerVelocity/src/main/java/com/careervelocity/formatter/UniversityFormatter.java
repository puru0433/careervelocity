package com.careervelocity.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.format.Formatter;

import com.careervelocity.models.University;

public class UniversityFormatter implements Formatter<University>{

	@Override
	public String print(University object, Locale locale) {
		return object.getUniverSityId()+"";
	}

	@Override
	public University parse(String text, Locale locale) throws ParseException {
		University university = new University();
		university.setUniverSityId(Long.parseLong(text));
		return university;
	}

}
