package com.careervelocity.formatter;
import java.text.ParseException;
import java.util.Locale;

import org.springframework.format.Formatter;

import com.careervelocity.models.Skills;
public class SkillFormatter implements Formatter<Skills>{

	@Override
	public String print(Skills skills, Locale arg1) {
		return skills.getSkillsId()+"";
	}

	@Override
	public Skills parse(String skillId, Locale arg1) throws ParseException {
		Skills skill = new Skills();
		skill.setSkillsId(Long.parseLong(skillId));
		return skill;
	}

}
